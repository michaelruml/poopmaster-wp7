﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Poopmaster.Models;

namespace Poopmaster.ViewModels
{
    public class SessionViewModel : BaseViewModel
    {
        public SessionViewModel()
        {
        }

        public SessionViewModel(Session session)
        {
            this.session = session;
        }

        private Session session;
        public Session Session
        {
            get
            {
                return session;
            }
            set
            {
                session = value;
                OnNotifyPropertyChanged("Session");
            }
        }

        public string Name
        {
            get
            {
                return session.Time.ToShortDateString();
            }
        }

        public string Description
        {
            get
            {
                return Session.Duration.Hours + ":"
                    + Session.Duration.Minutes + ":"
                    + Session.Duration.Seconds
                    + ", earned: " 
                    + session.Earned.ToString() + " " + "Kč";
            }
        }
        
    }
}

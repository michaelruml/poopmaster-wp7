﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Poopmaster.Models;

namespace Poopmaster.ViewModels
{
    public class OverviewViewModel : BaseViewModel
    {
        private ObservableCollection<Session> sessions;
        public ObservableCollection<Session> Sessions
        {
            get
            {
                return TestData();
                //return sessions;
            }
            set
            {
                sessions = value;
                OnNotifyPropertyChanged("Sessions");
            }
        }

        private ObservableCollection<Session> TestData()
        {
            ObservableCollection<Session> temp = new ObservableCollection<Session>();

            for (int i = 0; i < 10; i++)
            {
                Session session = new Session();
                session.Duration = new TimeSpan(0, 15, 23);
                session.Earned = 25;
                session.Time = new DateTime(2012, 10, 25);
                temp.Add(session);
            }

            return temp;
        }
    }
}

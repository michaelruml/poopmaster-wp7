﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Linq;
using Poopmaster.ViewModels;
using Poopmaster.Models;
using Poopmaster.support;


namespace Poopmaster.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private SessionProgressData progressData;
        private Sessions sessionsData;
        private Settings settingsData = StaticData.Settings;

        public ObservableCollection<SessionViewModel> Sessions { get; private set; }

        public MainViewModel()
        {
            loadData();
            initViewModels();
        }

        public void Backup()
        {
            PersistentDataStorage storage = new PersistentDataStorage();
            storage.Backup(DataManager.SessionProgressDataKey, progressData);
            storage.Backup(DataManager.SessionsKey, sessionsData);
        }

        private void initViewModels()
        {
            //progressData.IsSessionInProgress = false;
            //progressData.SessionInProgress = null;
            //sessionsData.SessionsList.Clear();
            //Backup();

            Sessions = new ObservableCollection<SessionViewModel>();

            if (sessionsData.SessionsList.Count == 0)
            {
                return;
            }

            foreach (Session s in sessionsData.SessionsList)
            {
                SessionViewModel viewModel = new SessionViewModel(s);
                Sessions.Add(viewModel);
            }
        }

        private void loadData()
        {
            progressData = DataManager.ProgressData();
            sessionsData = DataManager.Sessions();
        }



        public string LastSessionDuration 
        {
            get
            {
                if (sessionsData.SessionsList.Count == 0)
                {
                    return "00:00:00";
                }

                TimeSpan latest = (from s in sessionsData.SessionsList
                                  orderby s.Time descending
                                  select s).First().Duration;
                return string.Format("{0}:{1}:{2}", latest.Hours, latest.Minutes, latest.Seconds);
            }
        }

        private string earned;
        public string Earned
        {
            get
            {
                RecalculateEarned();
                return earned;
            }
            set
            {
                earned = value;
            }
        }

        private void RecalculateEarned()
        {
            if (sessionsData.SessionsList == null)
                return;

            Earned = (from session in sessionsData.SessionsList
                      select session.Earned).Sum().ToString() + " " + settingsData.Currency;
        }

        public bool IsSessionInProgress
        {
            get
            {
                return progressData.IsSessionInProgress;
            }
        }

        public void StartNewSession()
        {
            progressData.IsSessionInProgress = true;
            progressData.SessionInProgress = new Session();
            progressData.SessionInProgress.Time = DateTime.Now;
        }

        public void StopSession()
        {
            progressData.IsSessionInProgress = false;
            progressData.SessionInProgress.Duration = DateTime.Now.Subtract(progressData.SessionInProgress.Time);
            progressData.SessionInProgress.Earned = Math.Round(progressData.SessionInProgress.Duration.TotalHours * settingsData.Salary);
            SessionViewModel newSession = new SessionViewModel(progressData.SessionInProgress);
            Sessions.Add(newSession);
            sessionsData.SessionsList.Add(progressData.SessionInProgress);
            RecalculateEarned();
            Backup();
        }

        private ObservableCollection<SessionViewModel> TestData()
        {
            ObservableCollection<SessionViewModel> temp = new ObservableCollection<SessionViewModel>();

            for (int i = 0; i < 10; i++)
            {
                Session session = new Session();
                session.Duration = new TimeSpan(0, 15, 23);
                session.Earned = 25;
                session.Time = new DateTime(2012, 10, 25);

                SessionViewModel viewModel = new SessionViewModel();
                viewModel.Session = session;
                temp.Add(viewModel);
            }

            return temp;
        }
    }
}
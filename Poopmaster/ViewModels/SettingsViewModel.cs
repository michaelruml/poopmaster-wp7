﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Poopmaster.Models;
using Poopmaster.support;

namespace Poopmaster.ViewModels
{
    public class SettingsViewModel
    {
        private Settings settingsData;

        private string salary;
        public string Salary
        {
            get
            {
                return salary;
            }
            set
            {
                if (value.Length != 0)
                {
                    salary = value;
                    settingsData.Salary = (float)Convert.ToDouble(value);
                    Save();
                }
            }
        }

        private string currency;
        public string Currency
        {
            get
            {
                return currency;
            }
            set
            {
                if (value.Length != 0)
                {
                    currency = value;
                    settingsData.Currency = value;
                    Save();
                }
            }
        }

        public SettingsViewModel()
        {
            settingsData = DataManager.Settings();
            InitViewData();
        }

        private void InitViewData()
        {
            if (settingsData.Salary == 0)
                settingsData.Salary = 100;

            Salary = settingsData.Salary.ToString();

            if (settingsData.Currency == null || settingsData.Currency.Length == 0)
                settingsData.Currency = "Kč";

            Currency = settingsData.Currency;
        }

        public void Save()
        {
            PersistentDataStorage storage = new PersistentDataStorage();
            storage.Backup(DataManager.SettingsKey, settingsData);
        }
    }
}

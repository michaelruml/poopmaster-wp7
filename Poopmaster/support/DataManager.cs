﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Poopmaster.Models;
using Poopmaster.support;
using System.Collections.Generic;

namespace Poopmaster
{
    public static class DataManager
    {
        public static readonly string SessionProgressDataKey = "SessionProgressData";
        public static readonly string SessionsKey = "Sessions";
        public static readonly string SettingsKey = "Settings";

        public static Settings Settings()
        {
            Settings data = RestoreByType<Settings>(SettingsKey);

            if (data == null)
            {
                return new Settings();
            }

            return data;
        }

        public static SessionProgressData ProgressData()
        {
            SessionProgressData data = RestoreByType<SessionProgressData>(SessionProgressDataKey);

            if (data == null)
            {
                return new SessionProgressData();
            }

            return data;
        }

        public static Sessions Sessions()
        {
            Sessions sessions = RestoreByType<Sessions>(SessionsKey);

            if (sessions == null)
            {
                sessions = new Sessions();
            }

            if (sessions.SessionsList == null)
            {
                sessions.SessionsList = new List<Session>();
            }

            return sessions;
        }

        public static T RestoreByType<T>(string key)
        {
            PersistentDataStorage storage = new PersistentDataStorage();
            T obj = storage.Restore<T>(key);
            return obj;
        }
    }
}

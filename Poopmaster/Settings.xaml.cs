﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Poopmaster.ViewModels;

namespace Poopmaster
{
    public partial class Page1 : PhoneApplicationPage
    {
        private SettingsViewModel vm = new SettingsViewModel();

        public Page1()
        {
            InitializeComponent();
            DataContext = vm;
        }

        private void SalaryBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            vm.Salary = SalaryBox.Text;
        }

        private void CurrencyBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            vm.Currency = CurrencyBox.Text;
        }
    }
}
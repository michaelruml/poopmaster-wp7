﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Poopmaster.Models
{
    public class Session
    {
        public DateTime Time { get; set; }
        public TimeSpan Duration { get; set; }
        public double Earned { get; set; }
    }
}

﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Poopmaster.Models
{
    public class SessionProgressData
    {
        public bool IsSessionInProgress { get; set; }
        public Session SessionInProgress { get; set; }
    }
}
